#coding=utf-8

import tornado.web
import tornado.ioloop


class IndexHandler(tornado.web.RequestHandler):
    '''请求处理类'''
    def get(self):

        self.write('hello world!')

    def post(self):
        self.write(
            'hello world'
        )


if __name__ == '__main__':
    app = tornado.web.Application(
        [(r"/",IndexHandler)]
    )
    #服务器监听的端口
    app.listen(8000)
    tornado.ioloop.IOLoop.current().start()


